import requests
import math
import os
import time
import threading
import mimetypes
import sys

COSMETICS = 1

name = input("Name: ")
#name = "kanna"

url = input("Url: ")
#url = "https://i.kinja-img.com/gawker-media/image/upload/s--4LHBt0O4--/c_scale,f_auto,fl_progressive,q_80,w_800/kaprfadz9rnvypesa2u9.png"

partn = input("Thread count: ")
while 1:
    try:
        partn = int(partn)
    except:
        partn = input("That needs to be a number: ")
        continue

    if partn > 8:
        partn = input("Needs to be between 1 and 8: ")
        continue

    break

rh = requests.head(url)

try:
    max = rh.headers["content-length"]
except:
    print("Something went wrong")
    sys.exit()

parts = {}
threads = {}
queue = []
ext = mimetypes.guess_extension(rh.headers["content-type"])
name = "".join([name, ext])

print("")

def RequestAndDownload(x, r):
    global parts, name, threads, queue, partn, COSMETICS
    headers = { "Range": "bytes=" + r }
    resp = requests.get(url, headers = headers, stream = True)
    tot = int(r.split("-")[1]) - int(r.split("-")[0])

    chunks = 0
    downloaded = 0
    counter = 0
    parts[x] = b""
    _progress = 0
    for chunk in resp.iter_content(chunk_size = 512):
        chunks += 1
        downloaded = chunks * 512
        progress = str(int((downloaded / int(tot)) * 100))

        if COSMETICS == 1:
            queue.append([x, progress])
        else:
            if progress != _progress:
                queue.append([x, progress])

        _progress = progress
        parts[x] += chunk

    del threads[x]
    if len(threads) == 0:
        with open(os.getenv("HOME") + f"/Downloads/{name}", "ab") as f:
            for x in range(len(parts)):
                f.write(parts[x])


for x in range(partn):
    start = math.floor((int(max) / partn) * int(x)) + (1 if x > 0 else 0)
    end = math.floor((int(max) / partn) * (int(x) + 1))
    t = threading.Thread(target = RequestAndDownload, args = [range(partn)[x], str(start) + "-" + str(end)])
    threads[range(partn)[x]] = t
    t.start()

for x in range(len(threads)):
    print(f" Thread {x + 1}: 0%")

sys.stdout.write(f"\033[{partn}A\r")

cthread = 0
while len(queue) != 0 or len(threads) != 0:
    # iter queue
    if len(queue) > 0:
        thread = queue[0][0]
        percent = queue[0][1]
        moveTo = thread - cthread

        if moveTo < 0:
            cthread += moveTo
            moveTo = abs(moveTo)
            sys.stdout.write(f"\033[{moveTo}A\r")

        elif moveTo > 0:
            cthread += moveTo
            sys.stdout.write(f"\033[{moveTo}B\r")

        sys.stdout.write(f" Thread {thread + 1}: {percent}%\r")

        del queue[0]

sys.stdout.write(f"\033[{partn + 1}B\r")
print("\nDone!")
